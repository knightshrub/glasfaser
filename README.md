# Überblick

Es gibt mehrere Optionen für das Netzwerk-Setup.

Die Lösung die mir als erstes in den Sinn kam ist die, die von Deutsche Glasfaser
auch als fiber to the building (FTTB) bezeichnet wird, da im Haus selber dann
die Kupferverkabelung (Ethernet) verwendet wird.

Die anderen beiden Optionen wären ein fiber to the home (FTTH) setup, bei dem
der Glasfaser-Teilnehmeranschluss (GF-TA) bis in die Wohnung gezogen wird.
Dies ist das Setup, dass die Deutsche Glasfaser bewirbt, da es angeblich zukunftssicherer ist.

In einem Einfamilienhaus scheint mir die Unterscheidung zwischen FTTB und FTTH
nicht wirklich sinnvoll, weil Wohnung und Haus hier mehr oder weniger gleichbedeutend sind.
In einem Mehrfamilienhaus ist diese Unterscheidung allerdings sinnvoll, da jede Wohnung
ihren eigenen GF-TA bekommt.

Der Hausübergabepunkt (HÜP) wird immer im Keller sein, da dort das Glasfaserkabel
von der Straße ins Haus kommt. Zwischen HÜP und GF-TA liegt auch ein Glasfaserkabel
mit einer Länge von bis zu 20 m.

Wichtig zu wissen ist, dass das Glasfaser-Modem (network termination, NT) in einer
maximalen Entfernung von 0,5 m zum GF-TA installiert wird, d.h. neben der
Glasfaserdose hängt immer eine Plastikkiste die Glasfaser in Ethernet umwandelt.

Der Router wird **immer** über Ethernet angeschlossen.

Im folgenden eine kurze Beschreibung der drei möglichen Setups.

## FTTB

Die FTTB Variante, die mir zuerst in den Sinn kam ist wie folgt.
Der GF-TA und das NT kommen in den Keller, neben den HÜP.
Dort sind auch der Router und ein PoE switch der die WiFi access points mit
Strom versorgt.
In der Wohnung selber sind jeweils im Flur wo die Leerrohre sind zwei kleine
WiFi 6 access points zu sehen.
Durch die Leerrohre ziehen wir selber zwei Ethernetkabel.
Alle Geräte werden über WLAN mit dem Internet verbunden.

Diese Option verursacht in der Wohnung das wenigste Geraffel, da das alles
im Keller bleibt.
Nachteil wäre bei einem Hausverkauf, dass der Käufer bereit sein muss,
so ein komplizierteres Netzwerksetup weiterzubetreiben, da man nicht einfach 
eine Deutsche Glasfaser Router kaufen und im Flur einstöpseln kann und WLAN hat.

![](FTTB.svg)

## FTTH mit geraffel

Die erste FFTH Variante nach Deutsche Glasfaser wäre so, dass man vom HÜP
ein Glasfaserkabel bis in die Wohnung (Flur oben) ein Glasfaserkabel durch das
Leerrohr zieht. Der GF-TA kommt dann an die Wand neben dem Leerrohr. Hier
wird dann auch das NT sitzen. Zusätzlich würde man den Rest der Netzwerkinfrastruktur (Router, PoE switch)
auch hier beheimaten. Nachteil ist, dass man das ganze Geraffel irgendwo im
Flur unterbringen muss (Unordnung) und man eine Steckdose in der Nähe braucht, die
es dort aktuell nicht gibt.
Durch das Leerohr könnte man wieder ein Ethernetkabel in den Flur unten ziehen,
damit man dort auch noch einen WiFi access point hat.

![](FTTH_mit_geraffel.svg)

## FTTH mit wenig / ohne geraffel

Diese letzte Option ist eine Mischung der beiden vohrerigen Optionen.

Glasfaser wird vom HÜP wieder bis in den Flur oben zum GF-TA und NT gezogen.
Vom NT geht es per Ethernet wieder in den Keller zum Router und PoE switch und
von da aus wieder per Ethernet hoch in die Wohnung um die WiFi access points
zu versorgen.

Vorteil ist, dass bei einem Hausverkauf der Käufer einfach oben am NT einen
Deutsche Glasfaser 0815 Plasterouter einstöpseln kann und loslegen kann.

Weiterer Vorteil ist, dass wenigstens Router und PoE switch nicht im Flur 
rumhängen müssen.

![](FTTH_ohne_geraffel.svg)
